const React = require('react')

function mapValues (values, mapping) {
  if (mapping) {
    if (typeof mapping === 'function') {
      return Object.assign({}, mapping(...values))
    }
    return {[mapping]: values}
  }
  return Object.assign({}, ...values)
}

function mergeContextValuesWithProps (contextValues, props) {
  return Object.assign({}, contextValues, props)
}

function connect (mappingOrContext, ...Contexts) {
  return arguments.length === 1
    ? _connect(undefined, [mappingOrContext])
    : _connect(mappingOrContext, Contexts)
}

function _connect (mappings, Contexts) {
  return function (Component) {
    return function (props) {
      return (function factory (index, contextValues) {
        if (index === Contexts.length) {
          return React.createElement(
            Component,
            Object.assign(
              {},
              mergeContextValuesWithProps(mapValues(contextValues, mappings)),
              props),
            props.children || null)
        }
        return React.createElement(
          Contexts[index].Consumer,
          null,
          (ctx) => factory(index + 1, contextValues.concat(ctx)))
      })(0, [])
    }
  }
}

module.exports = {
  connect
}
