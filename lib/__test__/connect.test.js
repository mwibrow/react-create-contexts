'use strict'

const React = require('react')
const { renderToString } = require('react-dom/server')

const connect = require('../react-connect-contexts').connect

const TestContext = React.createContext()

const e = React.createElement

class TestComponent1 extends React.Component {
  render () {
    return e('div', {},
      e('span', null, this.props.children
        ? this.props.children(this)
        : null))
  }
}

class TestComponent2 extends React.Component {
  render () {
    return e('div', {},
      e('span', null, this.props.children
        ? this.props.children(this)
        : null))
  }
}

class TestComponent3 extends React.Component {
  render () {
    return e('div', {},
      e('span', null, this.props.children
        ? this.props.children(this)
        : null))
  }
}

function mapping (context) {
  return {
    mappedValue: context
  }
}

const ConnectedComponent = connect(TestContext)(TestComponent1)
const ConnectedComponentWithMapping = connect(mapping, TestContext)(TestComponent2)
const ConnectedComponentWithExactProperty = connect('exactProperty', TestContext)(TestComponent3)

describe('Test the connected component', () => {
  it('should contain the test value', () => {
    const testValue = 'test-value'
    const html = renderToString(
      e(TestContext.Provider, { value: { value: testValue } }, e(
        ConnectedComponent, null, function (obj) { return obj.props.value })))
    console.log(html)
    expect(html).toContain(testValue)
  })

  it.only('should contain the mapped test value', () => {
    const testValue = 'mapped-test-value'
    const html = renderToString(
      e(TestContext.Provider, { value: testValue }, e(
        ConnectedComponentWithExactProperty,
        null,
        function (obj) { return obj.props.exactProperty })))
    expect(html).toContain(testValue)
  })

  it('should contain the mapped test value', () => {
    const testValue = 'mapped-test-value'
    const html = renderToString(
      e(TestContext.Provider, { value: testValue }, e(
        ConnectedComponentWithMapping, null, function (obj) { return obj.props.mappedValue })))
    expect(html).toContain(testValue)
  })
})
