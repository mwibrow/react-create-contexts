import * as React from 'react'

interface IMapping {
  (...contexts: any[]): any | string | number
}

function mapValues (values: any[], mapping: IMapping): any {
  if (mapping) {
    if (typeof mapping === 'function') {
      return {...mapping(values)}
    }
    return {[mapping]: values}
  }
  return {...values}
}


function connect <C, P> (mapping: IMapping, ...Contexts: React.Context<C>[]) {
  return function (Component: React.SFC<P>) {
    return function (props: P) {
      return (function factory (index: number, values: any[]) {
        if (index === Contexts.length) {
          return <Component {...Object.assign({}, mapValues(values, mapping), props)} />
        }
        const Consumer: React.Consumer<C> = Contexts[index].Consumer
        return <Consumer>
          {ctx => factory(index + 1, values.concat(ctx))}
          </Consumer>
      })(0, [])
    }
  }
}

export {
  connect as default,
  connect
}