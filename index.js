"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
exports.__esModule = true;
var React = require("react");
function mapValues(values, mapping) {
    var _a;
    if (mapping) {
        if (typeof mapping === 'function') {
            return __assign({}, mapping(values));
        }
        return _a = {}, _a[mapping] = values, _a;
    }
    return __assign({}, values);
}
function connect(mapping) {
    var Contexts = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        Contexts[_i - 1] = arguments[_i];
    }
    return function (Component) {
        return function (props) {
            return (function factory(index, values) {
                if (index === Contexts.length) {
                    return React.createElement(Component, __assign({}, Object.assign({}, mapValues(values, mapping), props)));
                }
                var Consumer = Contexts[index].Consumer;
                return React.createElement(Consumer, null, function (ctx) { return factory(index + 1, values.concat(ctx)); });
            })(0, []);
        };
    };
}
exports["default"] = connect;
exports.connect = connect;
