# `react-connect-contexts`

The package provides a 'redux-style' `connect` function
for wrapping a component in (possibly multiple)
context consumers and optionally mapping context 
values to the component's props.

Note this package is *not* [`react-connect-context`](https://github.com/contiamo/react-connect-context).
This one has an `s` on the end and is currently pre-alpha.


## Basic usage

```
const connect = require('react-connect-contexts')
exampleContext = React.createContext()

class Example extends React.Component {
  render () {
    return (<span>{this.props.mappedValue}</span>)
  }
}

const mapContextToProps = (ctx) => {
  return {
    mappedValue: ctx.value
  }
}

const ExampleWithContext = connect(exampleContext, mapContextToProps)(Example)
```

